preferred_syntax = :scss

css_dir = "css"
sass_dir = "css/sass"

relative_assets = true
line_comments = true

require "autoprefixer-rails"

on_stylesheet_saved do |file|
  css = File.read(file)
  File.open(file, 'w') do |io|
    io << AutoprefixerRails.process(css, browsers: ["1%, last 2 versions, ie 8"])
  end
end